const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = merge(common, {
  entry: "./src/po.js",
  mode: "production",
  plugins: [htmlPlugin, new Dotenv({ path: "./poenv" })]
});
