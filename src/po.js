import React from "react";
import ReactDOM from "react-dom";

import { List, ListItem } from "framework7-react";

import Framework7 from "framework7/framework7.esm.bundle.js";
import Framework7React from "framework7-react";
import { AppTemplate } from "./components/AppTemplate.js";
import { AppContainer, RelContainer, save } from "mplus-react";
import { MaxList } from "./components/MaxList.js";
import { Section, QbeSection } from "./components/MaxSection.js";

import "framework7/css/framework7.min.css";

Framework7.use(Framework7React);

const tabs = [
  {
    title: "Search",
    tabLabel: "Search",
    tabIcon: "bag",
    actions: [
      {
        title: "Search",
        action: (ref, template) => {
          template.openTab(1);
          ref.search();
        }
      },
      {
        title: "Clear",
        action: (ref, template) => {
          ref.clear();
        }
      }
    ]
  },
  {
    title: "List",
    tabLabel: "List",
    tabIcon: "box",
    actions: []
  },
  {
    title: "Details",
    tabLabel: "PO",
    tabIcon: "compass",
    actions: [
      {
        title: "Change Status",
        action: (ref, template) => template.openDialog("changestatus")
      },
      {
        title: "Workflow",
        action: (ref, template) => template.openDialog("workflow")
      },
      {
        title: "Upload",
        action: (ref, template) => template.openDialog("fileupload")
      },
      {
        title: "Photo",
        action: (ref, template) => template.openDialog("photoupload")
      },
      {
        title: "Attachments",
        action: (ref, template) => template.openDialog("doclinks")
      },
      {
        title: "Save",
        action: (ref, template) => {
          save("pocont");
        }
      }
    ]
  }
];
// If you want to display icons in tab, the Framework7 icons are supported
// by default, you can find all the suported icons on their webpage
//        <QbeSection
//          container="pocont"
//          columns={["ponum", "description", "status",
// "shipvia", "orderdate"]}
//        />
let templateRef = null;
// reference to the templae, used to call the template functions from anywhere

const TestApp = () => (
  <>
    <AppTemplate tabs={tabs} ref={el => (templateRef = el)}>
      <QbeSection
        container="pocont"
        columns={["ponum", "description", "status", "shipvia", "orderdate"]}
        metadata={{
          SHIPVIA: { hasLookup: "true", listTemplate: "qbevaluelist" },
          STATUS: {
            hasLookup: "true",
            listTemplate: "qbevaluelist",
            filterTemplate: "valuelist"
          }
        }}
      />
      <MaxList
        container="pocont"
        columns={["ponum", "description", "status", "orderdate"]}
        norows="14"
        initdata="true"
        listTemplate="porow"
        showWaiting={true}
        selectableF={ev => templateRef.openTab(2)}
      />
      <Section
        container="pocont"
        columns={["ponum", "description", "status", "shipvia", "orderdate"]}
        metadata={{
          SHIPVIA: {
            hasLookup: "true",
            listTemplate: "valuelist",
            filterTemplate: "valuelist"
          }
        }}
      />
      <List simple-list inset>
        <ListItem title="Item 1" />
        <ListItem title="Item 2" />
        <ListItem title="Item 3" />
        <ListItem divider title="Pick a value" />
        <ListItem
          radio
          name="my-radio"
          value="1"
          title="Radio 1"
          defaultChecked
        />
        <ListItem radio name="my-radio" value="2" title="Radio 2" />
        <ListItem radio name="my-radio" value="3" title="Radio 3" />
      </List>
    </AppTemplate>

    <AppContainer mboname="po" appname="po" id="pocont" wfprocess="postatus" />
    <RelContainer container="pocont" relationship="poline" id="polinecont" />
  </>
);

ReactDOM.render(<TestApp />, document.getElementById("index"));
