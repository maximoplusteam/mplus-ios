import React from "react";
import { Section } from "./MaxSection.js";
import { Block, Row, Col, Button } from "framework7-react";
import { getWorkflowDialog } from "mplus-react";
import PropTypes from "prop-types";

/**
 * Implementation of the workflow warnings. It calls the Framework7 API after the dummy render to display the warnigns, this is why we need class
 */
class WFWarnings extends React.Component {
  /** Dummy render function
   * @return {null}
   */
  render() {
    return null;
  }
  /** Display the F7 warings , once they are pushed from Maximo
   * @param {object} props
   */
  componentDidUpdate(props) {
    if (this.props.warnings && this.props.warnings.length > 0) {
      let txt = "";
      for (let j = 0; j < this.props.warnings.length; j++) {
        if (j > 0) txt += "\n";
        txt += this.props.warnings[j];
      }
      const notification = this.$f7.notification.create({
        text: txt,
        closeTimeout: 4000
      });
      notification.open();
    }
  }
}

WFWarnings.propTypes = {
  warnings: PropTypes.array
};

export const WorkflowDialog = getWorkflowDialog(
  Section,
  (title, section, actions, warnings) => {
    const buttons = Object.keys(actions).map(key => (
      <Col key={key}>
        <Button onClick={actions[key].actionFunction}>
          {actions[key].label}
        </Button>
      </Col>
    ));

    return (
      <div>
        <Block key="wf-block">
          <p>{title}</p>
        </Block>
        <WFWarnings warnings={warnings} key="wf-warinings" />
        {section}
        <Row key="wf-buttons">{buttons}</Row>
      </div>
    );
  }
);
