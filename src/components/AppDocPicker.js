import React from "react";
import { getAppDocTypesPicker } from "mplus-react";
import { List, ListInput, Icon } from "framework7-react";
import PropTypes from "prop-types";

/** Picker class for document upload/download*/
class SimplePicker extends React.Component {
  /** Contructor, binds the changeValue function
   * @param {object} props React props
   */
  constructor(props) {
    super(props);
    this.changeValue = this.changeValue.bind(this);
    this.state = { value: null };
  }
  /**
   * changes local value
   * @param {string} value
   */
  changeValue(value) {
    this.setState({ value: value });
  }
  /** get local value
   * @return {string}
   */
  getValue() {
    return this.state.value || this.props.maxrows[0].data.DOCTYPE;
  }
  /**
   * Main render function
   * @return {React.ReactElement}
   */
  render() {
    if (!this.props.maxrows) return null;

    const litems = this.props.maxrows.map(({ data }) => (
      <option key={data.DOCTYPE} value={data.DOCTYPE}>
        {data.DOCTYPE}
      </option>
    ));
    return (
      <List>
        <ListInput
          label="Folder"
          defalutValue={this.props.maxrows[0].data.DOCTYPE}
          onChange={ev => this.changeValue(ev.target.value)}
          type="select"
        >
          <Icon f7="chevron-down" slot="media" />
          {litems}
        </ListInput>
      </List>
    );
  }
  /** React callback used to set the initial value of the picker
   * @param {object} prevProps
   * @param {object} prevState
   */
  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.maxrows && this.props.maxrows) {
      this.setState({ value: this.props.maxrows[0].DOCTYPE });
    }
  }
}

SimplePicker.propTypes = {
  maxrows: PropTypes.array
};

export const AppDocPicker = getAppDocTypesPicker(SimplePicker);
