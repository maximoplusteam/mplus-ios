import React from "react";
import { getSimpleList } from "mplus-react";

import { getListTemplate } from "./listTemplates.js";
import { List, ListItem, Icon, Block, Preloader } from "framework7-react";
import PropTypes from "prop-types";

const drawList = ({ data, listTemplate, rowAction }) => {
  const listTransformF = getListTemplate(listTemplate);
  if (!data) return null;
  const itemAction = mxrow => {
    rowAction(mxrow);
  };
  return data.map((r, i) => {
    const ltProps = listTransformF(r);
    if (r.icon) {
      return (
        <ListItem
          {...ltProps}
          onClick={ev => itemAction(r.mxrow)}
          mxrow={r.xmrow}
          key={r.key}
        >
          <Icon f7={r.icon} />
        </ListItem>
      );
    } else {
      return (
        <ListItem
          {...ltProps}
          onClick={ev => itemAction(r.mxrow)}
          mxrow={r.mxrow}
          key={r.key}
        />
      );
    }
  });
};
// MaxList is a component to be used on the top level. For a popup, we need a different component

const MPreloader = ({ waiting }) =>
  waiting ? (
    <Block className="text-align-center">
      <Preloader />
    </Block>
  ) : null;

MPreloader.propTypes = { waiting: PropTypes.bool };

const NoRows = ({ data, waiting }) =>
  !waiting && data && data.length == 0 ? (
    <div>
      <Block className="text-align-center">
        <Icon f7="alert" />
      </Block>
      <Block className="text-align-center">No values in the list</Block>
    </div>
  ) : null;

NoRows.propTypes = { waiting: PropTypes.bool, data: PropTypes.array };

const MPList = props => (
  <>
    <NoRows {...props} />
    <List>{drawList(props)}</List>
    <MPreloader {...props} />
  </>
);

MPList.propTypes = { props: PropTypes.object, data: PropTypes.array };

export const MaxList = getSimpleList(MPList);
