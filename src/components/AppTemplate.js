import React from "react";
import "framework7-icons";
import { ContextPool } from "react-multiple-contexts";

import {
  Link,
  Navbar,
  NavLeft,
  NavRight,
  NavTitle,
  App,
  Statusbar,
  Toolbar,
  View,
  Tabs,
  Tab,
  Page
} from "framework7-react";

import { shallowDiffers, setExternalRootContext } from "mplus-react";
import { DialogHolder } from "./dialogs.js";
import { MaximoLogin } from "./LoginPage.js";
import "maximoplus-core";
import PropTypes from "prop-types";
import $$ from "dom7";

maximoplus.net.globalFunctions.serverRoot = () => process.env.SERVER_ROOT;

let navbarSetF = null;

export const rootContext = React.createContext(null);
const navbarContext = React.createContext(null);

setExternalRootContext(rootContext);

/** Helper class for navbar*/
class NavBarHelper extends React.Component {
  /**
   * Main render function
   * @return {React.ReactElement}
   */
  render() {
    const Consumer = navbarContext.Consumer;
    return <Consumer>{value => value}</Consumer>;
  }
  /** React lifecycle function used to add the context to the navbar*/
  componentDidMount() {
    this.context.addInnerContext("navbar", navbarContext);
    navbarSetF = stateF => {
      this.context.setInnerState("navbar", stateF);
    };
  }
}
NavBarHelper.contextType = rootContext;

// I will use this context to render the navbar links based on the child components (the child component will update the state of the navbar context, as described in react-multiple-contexts). It will be set up only when the rootContext is setup from the multi-contexts lib

/** Main application template */
export class AppTemplate extends React.Component {
  /** Constructor, intializs the template
   * @param {object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      dialogs: [],
      needLogin: false,
      componentsTabData: {}
    };
    this.tabRefs = [];
    this.tabs = [];
    this.activeTab = 0;
    this.openTab = this.openTab.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.saveScrollPosition = this.saveScrollPosition.bind(this);
    this.fetching = false; // to prevent too fast data grabbing from the server, do it just on demand
    maximoplus.core.globalFunctions.global_login_function = err => {
      this.needLogin();
    };
    maximoplus.core.globalFunctions.handleErrorMessage = error => {
      this.$f7.dialog.alert(error, "Error");
    };

    maximoplus.core.globalFunctions.globalDisplayWaitCursor = () => {
      this.$f7.preloader.show();
    };

    maximoplus.core.globalFunctions.globalRemoveWaitCursor = () => {
      this.$f7.preloader.hide();
    };
  }
  /** Call when login is required*/
  needLogin() {
    if (!this.state.needLogin) {
      this.setState({ needLogin: true });
    }
  }
  /** Opens a dialog
   * @param {string} dialogType
   */
  openDialog(dialogType) {
    this.dialogHolder.openDialog({ type: dialogType });
  }
  /** React lifecycle component, used to prevent the unnecessary rendering
   * @param {object} nextProps
   * @param {object} nextState
   * @return {boolean}
   */
  shouldComponentUpdate(nextProps, nextState) {
    return (
      shallowDiffers(this.props, nextProps) ||
      shallowDiffers(this.state, nextState)
    );
  }
  /** Opens a tab in the template
   * @param {number} i - the tab to open
   */
  openTab(i) {
    this.activeTab = i;
    this.tabs[i].show(true);
  }
  /** gets inner navbar
   * @return {React.Element}
   */
  getNavBarInner() {
    const activeTabData = this.props.tabs && this.props.tabs[this.activeTab];
    const appTitle = activeTabData ? activeTabData.title : "";
    const navInner = [];

    const that = this;
    const aLeft = activeTabData && activeTabData.actions[0];
    const aRight = activeTabData && activeTabData.actions[1];
    // we will display maximum 2 links on the right side.
    const [, , ...actionsRest] = activeTabData.actions;
    let aRight2 = null;
    const aRef = that.tabRefs[that.activeTab].current;
    if (actionsRest.length == 0) {
      if (activeTabData.actions[2]) {
        aRight2 = activeTabData.actions[2];
      }
    } else {
      const actionButtons = actionsRest.map(a => {
        return {
          text: a.title,
          onClick: ev => a.action.call(this, aRef, this)
        };
      });

      const actions = this.$f7.actions.create({
        buttons: [...actionButtons, { text: "Cancel" }]
      });
      aRight2 = {
        title: "More",
        action: (ref, template) => actions.open()
      };
    }

    if (aLeft) {
      const lnk = { onClick: ev => aLeft.action.call(this, aRef, this) };
      if (aLeft.icon) lnk.iconF7 = aLeft.icon;
      const leftLink = <Link {...lnk}>{aLeft.icon ? "" : aLeft.title}</Link>;
      navInner.push(<NavLeft key="navleft">{leftLink}</NavLeft>);
    } else {
      navInner.push(<NavLeft key="navleft" />);
      // for f7 navbar,navleft and navright have to be on the page so title is in the midde
    }
    navInner.push(<NavTitle key="navtitle">{appTitle}</NavTitle>);
    if (aRight) {
      const lnk = { onClick: ev => aRight.action.call(this, aRef, this) };
      if (aRight.icon) lnk.iconF7 = aRight.icon;
      const rightLink = <Link {...lnk}>{aRight.icon ? "" : aRight.title}</Link>;

      navInner.push(<NavRight key="navrigght">{rightLink}</NavRight>);
    } else {
      navInner.push(<NavRight key="navright"/>);
    }

    if (aRight2) {
      const lnk = { onClick: ev => aRight2.action.call(this, aRef, this) };
      if (aRight2.icon) lnk.iconF7 = aRight2.icon;
      const rightLink = (
        <Link {...lnk}>{aRight2.icon ? "" : aRight2.title}</Link>
      );

      navInner.push(<NavRight key="navright">{rightLink}</NavRight>);
    }
    navbarSetF(_ => navInner);
    return navInner;
  }
  /**
   * Main render function
   * @return {React.ReactElement}
   */
  render() {
    let tabbarHasLabels = {};
    // we have to specify tabbar to have labels(icons) explicitely in F7

    const tabbarTabs = this.props.tabs ? (
      this.props.tabs.map((t, i) => {
        const _tabLink = "#tab-" + i;
        const tla = { tabLink: _tabLink };

        if (i == 0) {
          tla.tabLinkActive = true;
        }
        if (t.tabIcon) {
          //  tla.icon = "f7:" + t.tabIcon;
          tla.iconF7 = t.tabIcon;
          tabbarHasLabels = { labels: true };
        }

        return (
          <Link {...tla} key={_tabLink} onClick={ev => (this.activeTab = i)}>
            {t.tabLabel}
          </Link>
        );
      })
    ) : (
      <div />
    );

    const children = React.Children.map(this.props.children, (child, i) => {
      const newRef = React.createRef();
      this.tabRefs[i] = newRef;
      return React.cloneElement(child, {
        ref: newRef
      });
    });

    const pageTabs = this.props.tabs ? (
      this.props.tabs.map((t, i) => {
        const tla = { id: "tab-" + i };
        if (i == 0) {
          tla.tabActive = true;
        }

        // For the simple cases, we have the static actions in the tabs. If the action is on a child component (like filter for a List), and we steel need to display the action on the toolbar ( on the parent componant), we have to do the callback from the childcomponent, which will set the state on the template

        const Child = props => children[i];

        return (
          <Tab
            key={"tab-" + i}
            ref={el => (this.tabs[i] = el)}
            {...tla}
            onTabShow={ev => this.tabChanged.bind(this)(ev)}
            className="page-content"
          >
            <Child />
          </Tab>
        );
      })
    ) : (
      <div />
    );

    return (
      <ContextPool rootContext={rootContext} initialSize={10} minimumFree={3}>
        <App params={{ theme: "ios", name: "My Ap1p", id: "com.demoapp.test" }}>
          {/* Status bar overlay for full screen mode (Cordova or PhoneGap) */}
          <Statusbar />
          <View main>
            <Page pageContent={true} ref={page => (this.page = page)}>
              <Navbar ref={navbar => (this.navbar = navbar)}>
                <NavBarHelper />
              </Navbar>
              <Toolbar tabbar {...tabbarHasLabels}>
                {tabbarTabs}
              </Toolbar>
              <Tabs>{pageTabs}</Tabs>
            </Page>
          </View>
          <DialogHolder
            ref={dialogHolder => (this.dialogHolder = dialogHolder)}
          />
          <MaximoLogin open={this.state.needLogin} />
        </App>
      </ContextPool>
    );
  }
  /** Lifecycle React method, used to init infinite scrolling */
  componentDidMount() {
    this.initScrolling();
    if (!this.state.childrenRendered) {
      this.setState({ childrenRendered: true });
      // children actions will be available in refs just after the initial render, we need to render one more time to get them
    }
    this.getNavBarInner();
  }
  /** Lifecycle React method, used to init infinite scrolling
   * @param {object} prevProps
   * @param {object} prevState
   */
  componentDidUpdate(prevProps, prevState) {
    this.initScrolling();
  }
  /** Initialize the infinite scrolling*/
  initScrolling() {
    //      this.scrollingPage = this.page.refs.el.children[1];
    this.scrollingPage = this.page.refs.el.getElementsByClassName(
      "page-content"
    )[0];
    this.scrollingPositions = [];
    console.log(this.scrollingPage);

    $$(this.scrollingPage).on("scroll", this.saveScrollPosition);
    $$(this.scrollingPage).on("infinite", this.onInfinite.bind(this));
    this.$f7.infiniteScroll.create(this.scrollingPage);
  }
  /** Save the infinite scroll position between tabs navigation
   * @param {object} ev
   */
  saveScrollPosition(ev) {
    requestAnimationFrame(
      _ =>
        (this.scrollingPositions[this.activeTab] = this.scrollingPage.scrollTop)
    );
  }
  /** Actions to perform when the tab is changed
   * @param {object} ev - change tab event
   */
  tabChanged(ev) {
    const tabId = ev.target.id;
    const tabNo = parseInt(tabId.substring(tabId.lastIndexOf("-") + 1));
    if (this.scrollingPositions[tabNo]) {
      this.scrollingPage.scrollTop = this.scrollingPositions[tabNo];
    }
    this.activeTab = tabNo;
    this.getNavBarInner();
  }
  /** Function called on infinite scroll */
  onInfinite() {
    //    if (this.tabRefs[this.activeTab].current.state.fetching) return;
    const currTime = Date.now();
    if (!this.fetchTime || currTime - this.fetchTime > 1000) {
      this.fetchTime = currTime;
      const fetchF = this.tabRefs[this.activeTab].current.fetchMore;
      if (fetchF) fetchF(5);
    }
  }
}

AppTemplate.propTypes = {
  tabs: PropTypes.array,
  children: PropTypes.array
};
