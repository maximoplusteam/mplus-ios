import React from "react";
import {
  Popup,
  Page,
  Navbar,
  NavLeft,
  Link,
  NavTitle,
  Toolbar
} from "framework7-react";

import { getDialogHolder } from "mplus-react";
import PropTypes from "prop-types";
import $$ from "dom7";

const getDialog = require("./" + process.env.APP + "/dialog-templates.js")
  .getDialog;
// So far, this doesn't work with import, we have to user require

/** General Popup component used for displaying the dialogin this template*/
class WrappedPopup extends React.PureComponent {
  /** Contructor, binds the functions
   * @param {object} props React props
   */
  constructor(props) {
    super(props);
    this.state = { popupOpened: false, animating: false };
    this.dialogOpen = this.dialogOpen.bind(this);
    this.dialogClose = this.dialogClose.bind(this);
    this.startedOpening = this.startedOpening.bind(this);
    this.finishedOpening = this.finishedOpening.bind(this);
    this.onInfinite = this.onInfinite.bind(this);
  }
  /**
   * Setting the animation
   */
  startedOpening() {
    this.setState({ animating: true });
  }
  /**
       Stopping the animation and initializing the component
  */
  finishedOpening() {
    this.setState({ animating: false });
    const infContent = this.page.refs.el.children[2];
    $$(infContent).on("infinite", this.onInfinite);
    this.$f7.infiniteScroll.create(infContent);
  }
  /**
   * Infinite scroll handler
   */
  onInfinite() {
    const currTime = Date.now();
    if (
      this.currentRef &&
      this.currentRef.current &&
      this.currentRef.current.fetchMore &&
      (!this.fetchTime || currTime - this.fetchTime > 1000)
    ) {
      this.fetchTime = currTime;
      this.currentRef.current.fetchMore(5);
    }
  }
  /**
   * standard react
   */
  componentDidMount() {
    if (!this.page) return;
  }
  /**
   * Main render function
   * @return {React.ReactElement}
   */
  render() {
    const dialog = this.props.dialog;
    if (!dialog && !this.state.init) {
      // first render must not be popup, or it doesn't open,
      // looks like a bug in Framework7
      return <div />;
    }
    const [title, actions, Dialog] = dialog
      ? getDialog(dialog)
      : ["Close", null, null];
    let toolbar = <div />;
    const ref = React.createRef();
    this.currentRef = ref;
    const dlg =
      dialog && !this.state.animating ? (
        <Dialog {...dialog} ref={ref} />
      ) : (
        <div />
      );
    if (actions && actions.length > 0) {
      const innerT = actions.map(a => {
        if (a.action === "close") {
          return (
            <Link
              href="#"
              onClick={ev => {
                dialog.closeTheDialog();
              }}
            >
              {a.title}
            </Link>
          );
        } else {
          return (
            <Link href="#" onClick={_ => a.action.call(this, ref, this)}>
              {a.title}
            </Link>
          );
        }
      });
      toolbar = <Toolbar>{innerT}</Toolbar>;
    }
    return (
      <Popup
        opened={this.state.popupOpened}
        onPopupOpen={this.startedOpening}
        onPopupOpened={this.finishedOpening}
      >
        <Page ref={page => (this.page = page)}>
          <Navbar>
            <NavLeft>
              <Link
                iconF7="chevron_left"
                onClick={ev => {
                  dialog.closeTheDialog();
                }}
              >
                Back
              </Link>
            </NavLeft>
            <NavTitle>{title}</NavTitle>
          </Navbar>
          {toolbar}
          {dlg}
        </Page>
      </Popup>
    );
  }
  /**
   * Call this method to open the dialog
   */
  dialogOpen() {
    this.setState({ popupOpened: true, init: true });
  }
  /**
   * Call this method to close the dialog
   */
  dialogClose() {
    this.setState({ popupOpened: false });
    this.props.dialog.closeTheDialog();
  }
  /**
   * Internal method for opening the dialog from the MaximoPlus lib
   * @param {object} prevProps - previous component props
   * @param {object} prevState - previous component state
   */
  componentDidUpdate(prevProps, prevState) {
    if (this.state.popupOpened && prevProps.dialog && !this.props.dialog) {
      this.setState({ popupOpened: false });
    }
    if (!this.state.popupOpened && this.props.dialog && !prevProps.dialog) {
      this.setState({ popupOpened: true, init: true });
    }
  }
}

/** Implementation fo the dialog wrapper from mplus-react */
class DialogWrapper extends React.PureComponent {
  /** Constructor accepts the array of dialogs
   * @param {object} props contains the array of dialogs
   */
  constructor(props) {
    super(props);
    console.log("dialog wrapper constructor");
  }
  /** Main render function
   * @return {React.ReactElement}
   */
  render() {
    const currDialog =
      this.props.dialogs && this.props.dialogs[this.props.dialogs.length - 1];

    return (
      <WrappedPopup dialog={currDialog} ref={popup => (this.popup = popup)} />
    );
  }
}

WrappedPopup.propTypes = {
  dialog: PropTypes.object
};

DialogWrapper.propTypes = {
  dialogs: PropTypes.array
};

export const DialogHolder = getDialogHolder(DialogWrapper, null, true);
