import React from "react";
import "framework7-icons";
import { MaxList } from "../MaxList.js";
import { Section } from "../MaxSection.js";
import { WorkflowDialog } from "../WorkflowDialog.js";
import { DoclinksUploadDialog } from "../DoclinksUploadDialog.js";
import { PhotoUploadDialog } from "../PhotoUploadDialog.js";
import PropTypes from "prop-types";
import {
  RelContainer,
  SingleMboContainer,
  reload,
  getDoclinksViewer,
  getGLDialog
} from "mplus-react";

const ListDialog = React.forwardRef((d, ref) => (
  <MaxList
    norows="16"
    listTemplate={d.field.getMetadata().listTemplate}
    filterTemplate={d.field.getMetadata().filterTemplate}
    maxcontainer={d.listContainer}
    initdata="true"
    columns={d.dialogCols}
    selectableF={d.defaultAction}
    showWaiting={true}
    ref={ref}
  />
));

ListDialog.displayName = "ListDialog";

/*
Here you should define all the dialogs that is going to be used
 in the application.
Once the dialog is rendered, we need a way to execute the action
on the container of the dialog.
In this template action link is rendered outside the component,
 so we need the way to call the action on the container itself.
There are more ways to do it. We will use the standard React feature
 for this, ref forwaring. If you are not familiar with it, please read
about it in the React documentation. Ref will be passed from the dialog
 wrapper to the component itself, so we can execute the action on
 the container.
 */

const InvChangeStatus = React.forwardRef((props, ref) => (
  <div>
    <SingleMboContainer id="psd" container="invcont" />
    <RelContainer
      id="invchangestatus"
      container="psd"
      relationship="invchangestatus"
      ref={ref}
    />
    <Section
      container="invchangestatus"
      columns={["status", "memo"]}
      metadata={{
        STATUS: {
          hasLookup: "true",
          listTemplate: "valuelist",
          listColumns: ["value", "description"]
        }
      }}
    />
  </div>
));

InvChangeStatus.displayName = "InvChangeStatus";

/*
Status change functionality in Maximo requires the MboSet of the size of
 one object, this is why we have to user the SingleMboContainer.
 This is the special case, most of the time RelContainer is enough.
We need to refresh the main container after the statuschange is done
STATUS field on INVCHANGESTATUS object doesn't have the domain info,
domain is defined in the class, we need to put the fields
*/

const TransferItem = React.forwardRef((props, ref) => (
  <div>
    <RelContainer
      id="transferitemcont"
      container="invcont"
      relationship="transfercuritem"
      ref={ref}
    />
    <Section
      container="transferitemcont"
      columns={[
        "fromstoreloc",
        "tostoreloc",
        "fromlot",
        "tolot",
        "frombin",
        "tobin",
        "quantity",
        "conversion",
        "unitcost",
        "linecost",
        "gldebitacct",
        "glcreditacct",
        "memo"
      ]}
      metadata={{
        FROMSTORELOC: {
          listColumns: ["location", "description"],
          hasLookup: true,
          listTemplate: "location"
        },
        TOSTORELOC: {
          listColumns: ["location", "description"],
          hasLookup: true,
          listTemplate: "location"
        },
        GLDEBITACCT: { gl: true, hasLookup: true },
        GLCREDITACCT: { gl: true, hasLookup: true }
      }}
    />
  </div>
));

TransferItem.displayName = "TransferItem";

const CurrBalAdj = React.forwardRef((props, ref) => (
  <div>
    <RelContainer
      id="curbaladj"
      container="invcont"
      relationship="INVBALANCESNOSTGBIN"
      ref={ref}
    />
    <Section
      container="curbaladj"
      columns={[
        "binnum",
        "lotnum",
        "curbal",
        "newcurbal",
        "controlacc",
        "shrinkageacc"
      ]}
      metadata={{
        CONTROLACC: { gl: true, hasLookup: true },
        SHRINKAGEACC: { gl: true, hasLookup: true }
      }}
    />
  </div>
));

CurrBalAdj.displayName = "CurrBalAdj";

const InvWorkflowDialog = props => (
  <WorkflowDialog {...props} container="invcont" processname="INVSTATUS" />
);

// There is no workflow demo in standard maximo for the inventory. You have to define your own, and change the above process name

const InvDoclinksUploadDialog = React.forwardRef((props, ref) => (
  <DoclinksUploadDialog ref={ref} container="invcont" />
));

InvDoclinksUploadDialog.displayName = "InvDoclinksUploadDialog";

const InvPhotoUploadDialog = React.forwardRef((props, ref) => (
  <PhotoUploadDialog ref={ref} container="invcont" />
));

InvPhotoUploadDialog.displayName = "InvPhotoUploadDialog";

const DoclinksViewer = getDoclinksViewer(props => (
  <MaxList {...props} listTemplate="doclinks" />
));

const InvDoclinksViewer = React.forwardRef((props, ref) => (
  <DoclinksViewer ref={ref} container="invcont" />
));

InvDoclinksViewer.displayName = "InvDoclinksViewer";

const GlSegments = props => {
  const segments = props.segments.map(
    ({ listener, segmentName, segmentValue, segmentDelimiter }) => (
      <div
        style={{ display: "inline-block", marginRight: "3px" }}
        onClick={listener}
        key={segmentName}
      >
        <div style={{ fontSize: "8px" }}>{segmentName}</div>
        <div style={{ fontSize: "15px" }}>
          {segmentValue + segmentDelimiter}
        </div>
      </div>
    )
  );
  return <div>{segments}</div>;
};

GlSegments.propTypes = {
  segments: PropTypes.array
};

/** In order to run the action from the dialog, we create the virtual component to hold the ref*/
class GLDialogHelper extends React.Component {
  /** Constructor, binds the action
   * @param {object} props
   */
  constructor(props) {
    super(props);
    this.action = this.action.bind(this);
  }
  /** React render method
   * @return {React.ReactElement}
   */
  render() {
    return (
      <div>
        <GlSegments segments={this.props.segments} />
        {this.props.gllist}
      </div>
    );
  }
  /** To be called from the dialog menu, once the gl is chosen */
  action() {
    this.props.chooseF.call(this);
  }
}

GLDialogHelper.propTypes = {
  gllist: PropTypes.object,
  chooseF: PropTypes.func,
  segments: PropTypes.array
};

const drawDialog = (segments, gllist, chooseF, forwardedRef) => {
  return (
    <GLDialogHelper
      segments={segments}
      gllist={gllist}
      chooseF={chooseF}
      ref={forwardedRef}
    />
  );
};

const GLDialog1 = getGLDialog(drawDialog, MaxList);
const GLDialog = React.forwardRef((props, ref) => (
  <GLDialog1 {...props} forwardedRef={ref} N />
));
GLDialog.displayName = "GLDialog";

// action "close" is the special case, handled by template
const dialogTemplates = {
  list: dialog => ["Pick a value", null, ListDialog],
  qbelist: dialog => [
    "Pick one or more values",
    [{ title: "OK", action: "close" }],
    ListDialog
  ],
  gl: dialog => [
    "GL",
    [
      {
        title: "OK",
        action: (ref, dialogs) => {
          ref.current.action();
          dialogs.dialogClose();
        }
      },
      { title: "Cancel", action: "close" }
    ],
    GLDialog
  ],
  changestatus: dialog => [
    "Change Status",
    [
      {
        title: "OK",
        action: (ref, dialogs) => {
          const prom = ref.current.mboSetCommand("execute");
          dialogs.dialogClose();
          prom.then(_ => {
            reload("invcont");
            // dialog is on singleMboContainer, we need to manually reload
          });
        }
      },
      { title: "Cancel", action: "close" }
    ],
    InvChangeStatus
  ],
  currbaladj: dialog => [
    "Adjust Current Balance",
    [
      {
        title: "OK",
        action: (ref, dialogs) => {
          ref.current.mboSetCommand("execute");
          dialogs.dialogClose();
        }
      },
      { title: "Cancel", action: "close" }
    ],
    CurrBalAdj
  ],
  transferitem: dialog => [
    "TransferItem",
    [
      {
        title: "OK",
        action: (ref, dialogs) => {
          ref.current.mboSetCommand("execute");
          dialogs.dialogClose();
        }
      },
      { title: "Cancel", action: "close" }
    ],
    TransferItem
  ],
  workflow: dialog => ["Workflow", null, InvWorkflowDialog],
  fileupload: dialog => [
    "Upload Files",
    [
      {
        title: "Attach",
        action: (ref, dialogs) => {
          ref.current.attachFiles();
        }
      },
      {
        title: "Upload",
        action: (ref, dialogs) => {
          ref.current.uploadFiles();
        }
      }
    ],
    InvDoclinksUploadDialog
  ],
  photoupload: dialog => [
    "Upload Photo",
    [
      {
        title: "Camera",
        action: (ref, dialogs) => {
          ref.current.shoot();
        }
      },
      {
        title: "Upload",
        action: (ref, dialogs) => {
          ref.current.uploadPhoto();
        }
      },
      {
        title: "Remove",
        action: (ref, dialog) => {
          ref.current.removePhoto();
        }
      }
    ],
    InvPhotoUploadDialog
  ],
  doclinks: dialog => ["View Attachments", null, InvDoclinksViewer]
};

const Dummy = _ => <div />;
Dummy.displayName = "Dummy";

export const getDialog = d => {
  if (!d) return ["", null, Dummy];
  return dialogTemplates[d.type](d);
};
