import React from "react";
import { BlockTitle, List, ListItem } from "framework7-react";
import { getPickerList } from "mplus-react";

export const Picker = getPickerList(
  (label, selected, optionKey, optionVal, changeListener) => {
    return (
      <ListItem
        radio
        title={optionVal}
        value={optionKey}
        name={optionKey}
        checked={selected}
        onClick={ev => changeListener(optionKey)}
      />
    );
  },
  (label, changeListener, rows) => {
    return (
      <>
        <BlockTitle>{label}</BlockTitle>
        <List> {rows}</List>
      </>
    );
  }
);

// This Picker in Framework7 can be only standalone. For the pickers inside the dections, we have to do it inline (it can't have the picker drawn). Idea: for the section, just add the pickerrows along the other rows in the Section(the problem is how to control that)
