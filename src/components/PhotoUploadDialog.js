import React from "react";
import { getPhotoUpload } from "mplus-react";
import { AppDocPicker } from "./AppDocPicker.js";
import PropTypes from "prop-types";

const PhotoUpload = getPhotoUpload(props => <div>{props.children}</div>);

/** Class that uploads the photo from the mobile device to Maximo Doclinks
 */
export class PhotoUploadDialog extends React.Component {
  /** Constructor binds functions to be used from the template
   * @param {object} props
   */
  constructor(props) {
    super(props);
    this.uploadRef = React.createRef();
    this.pickerRef = React.createRef();
    this.elRef = React.createRef();
    this.uploadPhoto = this.uploadPhoto.bind(this);
  }
  /** Main render function
   * @return {React.ReactElement}
   */
  render() {
    return (
      <div ref={this.elRef}>
        <AppDocPicker container={this.props.container} ref={this.pickerRef} />
        <PhotoUpload {...this.props} ref={this.uploadRef} />
      </div>
    );
  }
  /** Method to upload the photo already taken */
  uploadPhoto() {
    const doctype = this.pickerRef.current.currentRef.current.adapterValue;
    this.uploadRef.current.uploadPhoto(doctype);
  }
  /** Takes a photo, but doesn't upload to the server */
  shoot() {
    this.uploadRef.current.shoot();
  }
  /** Remove the last taken photo, so we can make one more photo before upload */
  removePhoto() {
    this.uploadRef.current.removePhoto();
  }
}

PhotoUploadDialog.propTypes={
    container:PropTypes.string
};
