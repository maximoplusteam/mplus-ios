import React from "react";
import { getDoclinksUpload } from "mplus-react";
import { Block, Preloader } from "framework7-react";
import { AppDocPicker } from "./AppDocPicker.js";
import PropTypes from "prop-types";

const DoclinksUpload = getDoclinksUpload(props => {
  const fileNames = props.files.map((f, i) => {
    let name = f.name;
    if (props.finished && props.finished.indexOf(i) != -1) {
      name = name + " &#x2713;";
    }
    return <div key={name}>{name}</div>;
  });
  const errors = props.errors ? Object.values(props.errors) : [];
  const errDisp = errors.length > 0 ? "Errors" : "";
  const errArr = errors.map((e, i) => (
    <div key={"err" + i}>
      {i + 1}:{e}
    </div>
  ));

  return (
    <Block>
      <div key="dl-files">Files:</div>
      <div key="dl-filenames">{fileNames}</div>
      <div key="dl-errdisp">{errDisp}</div>
      <div key="dl-errarr">{errArr}</div>
      <div key="dl-preloader">{props.uploading ? <Preloader /> : ""}</div>
    </Block>
  );
});

/** Upload to Doclinks dialog */
export class DoclinksUploadDialog extends React.Component {
  /** Contructor, binds the functions
   * @param {object} props React props
   */
  constructor(props) {
    super(props);
    this.uploadRef = React.createRef();
    this.pickerRef = React.createRef();
    this.attachFiles = this.attachFiles.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
  }
  /**
   * Main render function
   * @return {React.ReactElement}
   */
  render() {
    return (
      <div>
        <AppDocPicker container={this.props.container} ref={this.pickerRef} />
        <DoclinksUpload {...this.props} ref={this.uploadRef} />
      </div>
    );
  }
  /** attach the files before the upload */
  attachFiles() {
    if (!this.props.uploading) {
      this.uploadRef.current.attachFiles();
    }
  }
  /** uploads file to the doclinks */
  uploadFiles() {
    if (!this.props.uploading) {
      const doctype = this.pickerRef.current.currentRef.current.adapterValue;
      this.uploadRef.current.uploadFiles(doctype);
    }
  }
}

DoclinksUploadDialog.propTypes = {
  container: PropTypes.string,
  uploading: PropTypes.bool
};
