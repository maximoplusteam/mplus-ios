import React from "react";
import "framework7-icons";
import { MaxList } from "./MaxList.js";
import { Section } from "./MaxSection.js";
import { WorkflowDialog } from "./WorkflowDialog.js";
import { DoclinksUploadDialog } from "./DoclinksUploadDialog.js";
import { PhotoUploadDialog } from "./PhotoUploadDialog.js";

import {
  RelContainer,
  SingleMboContainer,
  reload,
  getDoclinksViewer
} from "mplus-react";

const ListDialog = React.forwardRef((d, ref) => (
  <MaxList
    norows="16"
    listTemplate={d.field.getMetadata().listTemplate}
    filterTemplate={d.field.getMetadata().filterTemplate}
    maxcontainer={d.listContainer}
    initdata="true"
    columns={d.dialogCols}
    selectableF={d.defaultAction}
    showWaiting={true}
    ref={ref}
  />
));

ListDialog.displayName = "ListDialog";

/*
Here you should define all the dialogs that is going to be used
 in the application.
Once the dialog is rendered, we need a way to execute the action
on the container of the dialog.
In this template action link is rendered outside the component,
 so we need the way to call the action on the container itself.
There are more ways to do it. We will use the standard React feature
 for this, ref forwaring. If you are not familiar with it, please read
about it in the React documentation. Ref will be passed from the dialog
 wrapper to the component itself, so we can execute the action on
 the container.
 */

const POChangeStatus = React.forwardRef((props, ref) => (
  <div>
    <SingleMboContainer id="psd" container="pocont" />
    <RelContainer
      id="pochangestatus"
      container="psd"
      relationship="pochangestatus"
      ref={ref}
    />
    <Section
      container="pochangestatus"
      columns={["status", "memo"]}
      metadata={{
        STATUS: {
          hasLookup: "true",
          listTemplate: "valuelist",
          listColumns: ["value", "description"]
        }
      }}
    />
  </div>
));

POChangeStatus.displayName = "POChangeStatus";

/*
Status change functionality in Maximo requires the MboSet of the size of
 one object, this is why we have to user the SingleMboContainer.
 This is the special case, most of the time RelContainer is enough.
We need to refresh the main container after the statuschange is done
STATUS field on POCHANGESTATUS object doesn't have the domain info,
domain is defined in the class, we need to put the fields
*/

const POWorkflowDialog = props => (
  <WorkflowDialog {...props} container="pocont" processname="POSTATUS" />
);

const PODoclinksUploadDialog = React.forwardRef((props, ref) => (
  <DoclinksUploadDialog ref={ref} container="pocont" />
));

PODoclinksUploadDialog.displayName = "PODoclinksUploadDialog";

const POPhotoUploadDialog = React.forwardRef((props, ref) => (
  <PhotoUploadDialog ref={ref} container="pocont" />
));

POPhotoUploadDialog.displayName = "POPhotoUploadDialog";

const DoclinksViewer = getDoclinksViewer(props => (
  <MaxList {...props} listTemplate="doclinks" />
));

const PODoclinksViewer = React.forwardRef((props, ref) => (
  <DoclinksViewer ref={ref} container="pocont" />
));

PODoclinksViewer.displayName = "PODoclinksViewer";



// action "close" is the special case, handled by template
const dialogTemplates = {
  list: dialog => ["Pick a value", null, ListDialog],
  qbelist: dialog => [
    "Pick one or more values",
    [{ title: "OK", action: "close" }],
    ListDialog
  ],
  changestatus: dialog => [
    "Change Status",
    [
      {
        title: "OK",
        action: (ref, dialogs) => {
          const prom = ref.current.mboSetCommand("execute");
          dialogs.dialogClose();
          prom.then(_ => {
            reload("pocont");
            // dialog is on singleMboContainer, we need to manually reload
          });
        }
      },
      { title: "Cancel", action: "close" }
    ],
    POChangeStatus
  ],
  workflow: dialog => ["Workflow", null, POWorkflowDialog],
  fileupload: dialog => [
    "Upload Files",
    [
      {
        title: "Attach",
        action: (ref, dialogs) => {
          ref.current.attachFiles();
        }
      },
      {
        title: "Upload",
        action: (ref, dialogs) => {
          ref.current.uploadFiles();
        }
      }
    ],
    PODoclinksUploadDialog
  ],
  photoupload: dialog => [
    "Upload Photo",
    [
      {
        title: "Camera",
        action: (ref, dialogs) => {
          ref.current.shoot();
        }
      },
      {
        title: "Upload",
        action: (ref, dialogs) => {
          ref.current.uploadPhoto();
        }
      },
      {
        title: "Remove",
        action: (ref, dialog) => {
          ref.current.removePhoto();
        }
      }
    ],
    POPhotoUploadDialog
  ],
  doclinks: dialog => ["View Attachments", null, PODoclinksViewer]
};

const Dummy = _ => <div />;
Dummy.displayName = "Dummy";

export const getDialog = d => {
  if (!d) return ["", null, Dummy];
  return dialogTemplates[d.type](d);
};
