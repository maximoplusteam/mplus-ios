import React from "react";
import "framework7-icons";
import PropTypes from "prop-types";

import {
  Page,
  LoginScreenTitle,
  List,
  ListInput,
  ListButton,
  Popup,
  BlockFooter
} from "framework7-react";

/** Simple Login Components, called when the session is not present*/
export class MaximoLogin extends React.Component {
  /** Contructor, defines username and password
   * @param {object} props React props
   */
  constructor(props) {
    super(props);
    this.state = { username: "", password: "" };
  }
  /**
   * Main render function
   * @return {React.ReactElement}
   */
  render() {
    const popup = (
      <Popup opened={true}>
        <Page loginScreen noToolbar noNavbar noSwipeback>
          <LoginScreenTitle>MaximoPlus</LoginScreenTitle>
          <List form>
            <ListInput
              label="Username"
              type="text"
              placeholder="Your username"
              value={this.state.username}
              onInput={e => {
                this.setState({ username: e.target.value });
              }}
            />
            <ListInput
              label="Password"
              type="password"
              placeholder="Your password"
              value={this.state.password}
              onInput={e => {
                this.setState({ password: e.target.value });
              }}
            />
          </List>
          <List>
            <ListButton onClick={ev => this.loginAction()}>Sign In</ListButton>
            <BlockFooter>Enter your Maximo credentials</BlockFooter>
          </List>
        </Page>
      </Popup>
    );
    return <div>{this.props.open ? popup : <div />}</div>;
  }
  /**
   * Login action to call the login on the server
   */
  loginAction() {
    maximoplus.core.max_login(
      this.state.username,
      this.state.password,
      _ => document.location.reload(),
      err =>
        maximoplus.core.globalErrorHandler(["Invalid Username or Password"])
    );
  }
}

MaximoLogin.propTypes = {
  open: PropTypes.bool
};
