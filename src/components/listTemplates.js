/*
For Framefork7 React, we have to pass the ListItem DIRECTLY under the list,
 that means we can't return the components here (ListItem). ListItem will
 be rendered in the MaxList component directly. We will return just the
 properties for the ListItem. If we pass the template like
(props)=><ListItem {...props}/>
that will create another component, and it will not render correctly
*/
const listTemplates = {
  valuelist: props => ({
    header: props.data.VALUE,
    title: props.data.DESCRIPTION
  }),
  qbevaluelist: props => ({
    header: props.data.VALUE,
    title: props.data.DESCRIPTION,
    icon: props.data._selected === "Y" ? "check" : ""
  }),
  porow: props => ({
    header: props.data.PONUM,
    after: props.data.STATUS,
    footer: props.data.ORDERDATE,
    title: props.data.DESCRIPTION,
    link: "#"
  }),
  doclinks: props => ({
    header: props.data.DOCUMENT,
    title: props.data.DESCRIPTION,
    footer: props.data.DOCTYPE
  }),
  invrow: props => ({
    header: props.data.ITEMNUM,
    title: props.data["ITEM.DESCRIPTION"],
    footer: props.data.VENDOR,
    after: props.data.ORDERUNIT,
    link: "#"
  }),
  company: props => ({
    header: props.data.COMPANY,
    title: props.data.NAME,
    footer: props.data.CONTACT,
    after: props.data.PHONE
  }),
  qbecompany: props => ({
    header: props.data.COMPANY,
    title: props.data.NAME,
    footer: props.data.CONTACT,
    after: props.data.PHONE,
    icon: props.data._selected === "Y" ? "check" : ""
  }),
  unit: props => ({
    header: props.data.MEASUREUNITID,
    title: props.data.DESCRIPTION
  }),
  qbeunit: props => ({
    header: props.data.MEASUREUNITID,
    title: props.data.DESCRIPTION,
    icon: props.data._selected === "Y" ? "check" : ""
  }),
  location: props => ({
    header: props.data.LOCATION,
    title: props.data.DESCRIPTION
  }),
  qbelocation: props => ({
    header: props.data.LOCATION,
    title: props.data.DESCRIPTION,
    icon: props.data._selected === "Y" ? "check" : ""
  }),
  gllist: props => ({
    header: props.data.COMPVALUE,
    title: props.data.COMPTEXT
  })
};

export const getListTemplate = templateId => listTemplates[templateId];
