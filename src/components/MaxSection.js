import React from "react";
import { getSection, getQbeSection, getPickerList } from "mplus-react";
import { List, ListInput, Icon, ListItem } from "framework7-react";
import PropTypes from "prop-types";

const RadioButton = getPickerList(
  (label, selected, optionKey, optionVal, changeListener) => (
    <ListItem title={optionVal} onClick={ev => changeListener(optionKey)}>
      {selected ? <Icon slot="inner" f7="check" /> : null}
    </ListItem>
  ),
  (label, changeListener, rows) => (
    <ul>
      <ListItem divider title={label} />
      {rows}
      <ListItem divider />
    </ul>
  )
);

const textField = (props, key, calendarF) => {
  if (props.kind && props.kind == "picker") {
    return <RadioButton {...props} />;
  }
  let lookup = "";
  if (props.showLookupF && typeof props.showLookupF === "function") {
    lookup = (
      <div slot="media" onClick={ev => props.showLookupF()}>
        <Icon f7="search" slot="media" />
      </div>
    );
  }

  if (props.type == "DATE" || props.type == "DATETIME") {
    lookup = (
      <div
        slot="media"
        onClick={ev => {
          const calendar = calendarF.create({
            routableModals: false,
            dateFormat: "dd-M-yyyy"
          });
          calendar.on("change", (cal, value) => {
            if (props.changeListener) {
              props.immediateChangeListener(value[0]);
            } else {
              props.listener(value[0]);
            }
            cal.close();
          });
          calendar.open();
        }}
      >
        <Icon f7="calendar" />
      </div>
    );
  }

  if (props.metadata && props.metadata.phonenum) {
    lookup = (
      <div
        slot="media"
        onClick={ev => {
          if (!props.value) return;
          window.open("tel:" + props.value);
        }}
      >
        <Icon f7="phone" />
      </div>
    );
  }
  if (props.metadata && props.metadata.smsnum) {
    lookup = (
      <div
        slot="media"
        onClick={ev => {
          if (!props.value) return;
          window.open("sms:" + props.value);
        }}
      >
        <Icon f7="message" />
      </div>
    );
  }
  if (props.metadata && props.metadata.barcode) {
    lookup = (
      <div
        slot="media"
        onClick={ev => {
          cordova.plugins.barcodeScanner.scan(
            result => {
              props.changeListener(result.text);
            },
            error => {
              console.log(error);
            }
          );
        }}
      >
        <Icon f7="barcode" />
      </div>
    );
  }
  return (
    <ListInput
      key={props.fieldKey}
      label={props.label}
      onChange={ev => props.listener(ev.target.value)}
      onBlur={ev => {
        // for qbe no changeLstener
        if (props.changeListener) {
          props.changeListener();
        }
      }}
      readonly={!props.enabled}
      required={props.required}
      value={props.value == null ? "" : props.value}
      type="text"
    >
      {lookup}
    </ListInput>
  );
};

textField.propTypes = {
  type: PropTypes.string,
  showLookupF: PropTypes.func,
  listener: PropTypes.func,
  changeListener: PropTypes.func,
  immediateChangeListener: PropTypes.func,
  enabled: PropTypes.bool,
  required: PropTypes.bool,
  fieldKey: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string,
  kind: PropTypes.string,
  metadata: PropTypes.object
};

/** Helper class to pass the $f7 instance to fields*/
class ListOfFields extends React.Component {
  /** Contructor, create the ref
   * @param {object} props React props
   */
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }
  /**
   * Main render function
   * @return {React.ReactElement}
   */
  render() {
    const calendarF =
      this.ref && this.ref.current && this.ref.current.$f7.calendar;
    return (
      <List ref={this.ref} inlineLabels noHairlinesMd inset>
        {this.props.flds.map((f, i) => textField(f, i, calendarF))}
      </List>
    );
  }
}

ListOfFields.propTypes = {
  flds: PropTypes.array
};

export const Section = getSection(null, null, flds => (
  <ListOfFields flds={flds} />
));

export const QbeSection = getQbeSection(
  null,
  (fields, buttons) => {
    return <ListOfFields flds={fields} />;
  },
  buttons => {
    return {
      actionLeft: { title: buttons[0].label, key: buttons[0].key },
      actionRight: { title: buttons[1].label, key: buttons[1].key }
    };
  }
); // buttons will be virtual (like fields, so no need to create the components, data is enough
