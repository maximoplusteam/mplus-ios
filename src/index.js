import React from "react";
import ReactDOM from "react-dom";
import Framework7 from "framework7/framework7.esm.bundle.js";
import Framework7React from "framework7-react";
import { AppTemplate } from "./components/AppTemplate.js";
import { AppContainer, save } from "mplus-react";
import { MaxList } from "./components/MaxList.js";
import { Section, QbeSection } from "./components/MaxSection.js";

import "framework7/css/framework7.min.css";
import "./overrides.css";

Framework7.use(Framework7React);

const tabs = [
  {
    title: "Filter",
    tabLabel: "Filter",
    tabIcon: "filter",
    actions: [
      {
        title: "Search",
        action: (ref, template) => {
          template.openTab(1);
          ref.search();
        }
      },
      {
        title: "Clear",
        action: (ref, template) => {
          ref.clear();
        }
      }
    ]
  },
  {
    title: "List",
    tabLabel: "List",
    tabIcon: "list",
    actions: []
  },
  {
    title: "Details",
    tabLabel: "Inventory",
    tabIcon: "box",
    actions: [
      {
        title: "Save",
        action: (ref, template) => {
          save("invcont");
        }
      },
      {
        title: "Change Status",
        action: (ref, template) => template.openDialog("changestatus")
      },
      {
        title: "Transfer Item",
        action: (ref, template) => template.openDialog("transferitem")
      },
      {
        title: "Current Balance Adjustment",
        action: (ref, template) => template.openDialog("currbaladj")
      },
      {
        title: "Upload",
        action: (ref, template) => template.openDialog("fileupload")
      },
      {
        title: "Photo",
        action: (ref, template) => template.openDialog("photoupload")
      },
      {
        title: "Attachments",
        action: (ref, template) => template.openDialog("doclinks")
      }
    ]
  }
];
// If you want to display icons in tab, the Framework7 icons are supported
// by default, you can find all the suported icons on their webpage
//        <QbeSection
//          container="invcont"
//          columns={["ponum", "description", "status",
// "shipvia", "orderdate"]}
//        />
let templateRef = null;
// reference to the templae, used to call the template functions from anywhere

const TestApp = () => (
  <>
    <AppTemplate tabs={tabs} ref={el => (templateRef = el)}>
      <QbeSection
        container="invcont"
        columns={[
          "itemnum",
          "item.description",
          "internal",
          "abctype",
          "manufacturer",
          "modelnum",
          "orderunit",
          "vendor",
          "issueunit",
          "location"
        ]}
        metadata={{
          ABCTYPE: { hasLookup: "true", listTemplate: "qbevaluelist" },
          MANUFACTURER: {
            hasLookup: "true",
            listColumns: ["company", "name", "contact", "phone"],
            listTemplate: "qbecompany"
          },
          ORDERUNIT: {
            hasLookup: "true",
            listTemplate: "qbeunit",
            listColumns: ["measureunitid", "description"]
          },
          ISSUEUNIT: {
            hasLookup: "true",
            listTemplate: "qbeunit",
            listColumns: ["measureunitid", "description"]
          },
          VENDOR: {
            hasLookup: true,
            listColumns: ["company", "name", "contact", "phone"],
            listTemplate: "qbecompany"
          },
          LOCATION: {
            hasLookup: "true",
            listColumns: ["location", "description"],
            listTemplate: "qbelocation"
          }
        }}
      />
      <MaxList
        container="invcont"
        columns={[
          "manufacturer",
          "modelnum",
          "orderunit",
          "vendor",
          "issueunit",
          "itemnum",
          "item.description",
          "location"
        ]}
        norows="14"
        initdata="true"
        listTemplate="invrow"
        showWaiting={true}
        selectableF={ev => templateRef.openTab(2)}
      />
      <Section
        container="invcont"
        columns={[
          "itemnum",
          "item.description",
          "item.rotating",
          "internal",
          "abctype",
          "manufacturer",
          "modelnum",
          "orderunit",
          "vendor",
          "vendor.primarycontact.voicephone",
          "issueunit",
          "location",
          "glaccount"
        ]}
        metadata={{
          ABCTYPE: { hasLookup: "true", listTemplate: "valuelist" },
          MANUFACTURER: {
            hasLookup: "true",
            listColumns: ["company", "name", "contact", "phone"],
            listTemplate: "company"
          },
          ORDERUNIT: {
            hasLookup: "true",
            listTemplate: "unit",
            listColumns: ["measureunitid", "description"]
          },
          ISSUEUNIT: {
            hasLookup: "true",
            listTemplate: "unit",
            listColumns: ["measureunitid", "description"]
          },
          VENDOR: {
            hasLookup: true,
            listColumns: ["company", "name", "contact", "phone"],
            listTemplate: "company"
          },
          LOCATION: {
            hasLookup: "true",
            listColumns: ["location", "description"],
            listTemplate: "location"
          },
          GLACCOUNT: { hasLookup: "true", gl: "true" },
          "VENDOR.PRIMARYCONTACT.VOICEPHONE": { phonenum: "true" }
        }}
      />
    </AppTemplate>

    <AppContainer mboname="inventory" appname="inventor" id="invcont" />
  </>
);

ReactDOM.render(<TestApp />, document.getElementById("index"));
