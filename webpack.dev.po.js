const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  entry: "./src/po.js",
  mode: "development",
  devtool: "inline-source-map"
});
